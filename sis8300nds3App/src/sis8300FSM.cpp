#include "sis8300FSM.h"

sis8300FSM::sis8300FSM(const std::string &name,	nds::Node &parentNode, 
        sis8300drv_usr &deviceUser) : 
    m_node(nds::Port(name, nds::nodeType_t::generic)),
    m_deviceUser(deviceUser),
    // INFO PV
    m_infoPV(nds::PVDelegateIn<std::string>(
        "InfoMessage",
        std::bind(&sis8300FSM::getInfoMessage, this,
            std::placeholders::_1, std::placeholders::_2))),
    // Firmware version PV
    m_firmwareVerPV(nds::PVDelegateIn<std::string>(
        "FirmwareVersion",
        std::bind(&sis8300FSM::getFirmwareVersion, this,
            std::placeholders::_1, std::placeholders::_2)))
{
	ndsDebugStream(m_node) << "[LLRFIOC] sis8300FSM constructor " << std::endl;

	struct timespec now = {0, 0};
	clock_gettime(CLOCK_REALTIME, &now);

	parentNode.addChild(m_node);


	//-----------------------------------------------------------------------------
	// Start of PV objects construction and addition to the tree
	//-----------------------------------------------------------------------------
  	// PV for debug/info messages.
	m_infoPV.setScanType(nds::scanType_t::interrupt);
	m_infoPV.setMaxElements(512);
	m_node.addChild(m_infoPV);

  	// PV for firmware version 
    m_firmwareVerPV.setScanType(nds::scanType_t::interrupt);
    m_firmwareVerPV.setMaxElements(20);
	m_node.addChild(m_firmwareVerPV);

  	// PVs for NDS state machine.
	m_stateMachine = m_node.addChild(nds::StateMachine(
		true, std::bind(&sis8300FSM::onSwitchOn, this),
		std::bind(&sis8300FSM::onSwitchOff, this),
		std::bind(&sis8300FSM::onStart, this),
		std::bind(&sis8300FSM::onStop, this),
		std::bind(&sis8300FSM::recover, this),
		std::bind(&sis8300FSM::allowChange, this,
			std::placeholders::_1, std::placeholders::_2,
			std::placeholders::_3)));
	m_stateMachine.setLogLevel(nds::logLevel_t::none);

	//-----------------------------------------------------------------------------
	// End of PV objects construction
	//-----------------------------------------------------------------------------

}

//-----------------------------------------------------------------------------
//  Destructor
//-----------------------------------------------------------------------------
sis8300FSM::~sis8300FSM() {
	ndsDebugStream(m_node) << "[LLRFIOC] Destroying sis8300FSM" << std::endl;
}


//Function migrated from onEnterInit from NDS2 implementation (sis8300llrf)
//It should contain the steps done when the LLRF FSM goes to RESET
//TODO: Review if this should be here on in a new class sis8300FSM
void sis8300FSM::onSwitchOn(){
    int status;
    unsigned fwMajor, fwMinor, fwPatch, value;
    std::string serial;

    struct timespec now = {0, 0};

	ndsDebugStream(m_node) << "[LLRFIOC] sis8300FSM Switched On" << std::endl;

    clock_gettime(CLOCK_REALTIME, &now);

    /* Try to open the device node specified at driver init. If this fails
     * put the device into ERROR. */
    status = sis8300drv_open_device(&m_deviceUser);
    SIS8300NDS_STATUS_CHECK("sis8300drv_open_device", status);


    //firmware version used to determine if certain registers are required for operation 
    status = sis8300llrfdrv_get_fw_version(&m_deviceUser, &fwMajor, &fwMinor, &fwPatch);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_fw_version", status);

    //push firmware version to th ePV
    m_firmwareVersion = std::to_string(fwMajor) + "." + std::to_string(fwMinor) + "." + std::to_string(fwPatch);

    ndsDebugStream(m_node)  << "Version:" << m_firmwareVersion << std::endl;
    m_firmwareVerPV.push(now, m_firmwareVersion);


    //READ FIRMWARE INFORMATION - INIT - TODO

   // Read fw/serial info from card and update records. 
   status = sis8300drv_get_serial(&m_deviceUser, &value);
   SIS8300NDS_STATUS_CHECK("sis8300drv_get_serial", status);
   // NDS_DBG("sis8300drv_get_serial returned %u.", value);

   serial = std::to_string(value);
   ndsDebugStream(m_node) << "Serial: " << value << std::endl;
   // snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%u", value);
   // _serial = buffer;
   // doCallbacksOctetStr(_serial, ASYN_EOM_END, _interruptIdSerial);

    ////sis8300drv_get_fw_version gets firmware version and model information
    //status = sis8300drv_get_fw_version(&m_deviceUser, &value);
    //SIS8300NDS_STATUS_CHECK("sis8300drv_get_fw_version", status);
    //NDS_DBG("sis8300drv_get_fw_version returned 0x%X.", value);

    //snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%X", value >> 16);
    //_model = buffer;
    //doCallbacksOctetStr(_model, ASYN_EOM_END, _interruptIdModel, _portAddr);

    //READ FIRMWARE INFORMATION - END

    /* To be sure of the device state, does not hurt */
    status = sis8300llrfdrv_sw_reset(&m_deviceUser);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_sw_reset", status);

    /* Initialize the board */
    status = sis8300llrfdrv_mem_ctrl_set_custom_mem_map(&m_deviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_mem_ctrl_set_custom_mem_map", status);
    
    status = sis8300drv_init_adc(&m_deviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_init_adc", status);

    status = sis8300llrfdrv_setup_dac(&m_deviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_setup_dac", status);

//TODO: review
//    /* Initialize the generic NDS part
//     * parent onEnterInit handler does this. But it also initializes the DAC and ADC and
//     * we want to prevent this here. So we copy this part of AI channel group initialization
//     * from the parent */
//    if (_CgAI != NULL) {
//        NDS_DBG("Initializing channel group %s.", _CgAI->getName().c_str());
//        statusSamplesCount = _CgAI->checkSamplesConfig(-1, -1, &samplesCountMax);
//        if (statusSamplesCount != ndsSuccess) {
//            _CgAI->setSamplesCount(NULL, samplesCountMax);
//        }
//        _CgAI->markAllParametersChanged();
//        _CgAI->commitParameters();
//        _CgAI->commitAllChannels();
//    }

//    /* Initialize the llrf NDS part */
//    if (from == nds::DEVICE_STATE_OFF) {
//        
//        for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
//            if (_llrfChannelGroups[i]->initialize() != ndsSuccess) {
//                NDS_ERR("Could not initialize CG %s", _llrfChannelGroups[i]->getName().c_str());
//                SIS8300NDS_MSGERR("Could not initialize CG");
//                error();
//                return ndsError;
//            }
//            else {
//                NDS_INF("Sucessfully inititalized CG %s", _llrfChannelGroups[i]->getName().c_str());
//            }
//        }
//
//    }
//    else if (from == nds::DEVICE_STATE_RESETTING) {
//
//        /* When CGs and CHs enter the disabled state they will rewrite all
//         * the values to the controller */
//        llrfCgStateSet(nds::CHANNEL_STATE_DISABLED);
//    }
//    else {
//        NDS_ERR("Arriving to INIT from an unsupported state, %i", (int) from);
//        SIS8300NDS_MSGERR("Arriving to INIT from an unsupported state");
//        error();
//        return ndsError;
//    }
//
//    //This should be here because the loop mode should be set after the setOperatin Mode
//    if (_CgCtrl != NULL) {
//        _CgCtrl->markAllParametersChanged();
//        _CgCtrl->commitParameters();
//    } 

//    //Mark device parameters as changed
//    _MaxRFLengthChange = 1;
//    _MaxDAQLengthChange = 1;
//    _LPSDeadTimeChange = 1;

//    //commit parameters from Device
//    commitParameters();

    //TODO: move from original sis8300Device
//	sis8300Device::restoreAllValues();

    //TODO: review
//    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());

    //TODO:
//    return getFirmwareStatus();

}

//TODO: onStart : The place where we start the acquisition thread
// The place where we move all groups and channels to running

//TODO: acquisition thread

//TODO: onStop

//TODO: add timers to periodically reading



//-----------------------------------------------------------------------------
// NDs state machine: when switching to OFF
//-----------------------------------------------------------------------------
void sis8300FSM::onSwitchOff() {

}

//-----------------------------------------------------------------------------
// NDs state machine: when switching to RUNNING
//-----------------------------------------------------------------------------
void sis8300FSM::onStart() {

}

//-----------------------------------------------------------------------------
// NDs state machine: when switching from RUNNING to STOP
//-----------------------------------------------------------------------------
void sis8300FSM::onStop() {
	
}

//-----------------------------------------------------------------------------
// NDs state machine: helper functions
// TODO: how to use this properly?
//-----------------------------------------------------------------------------
void sis8300FSM::recover() {
	throw nds::StateMachineRollBack("Cannot recover");
}
bool sis8300FSM::allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal) {
	return true;
}

void sis8300FSM::getFirmwareVersion(timespec *timespec, std::string *value){
    clock_gettime(CLOCK_REALTIME, timespec);
    *value = m_firmwareVersion;
}


void sis8300FSM::getInfoMessage(timespec *timespec, std::string *value) {
    clock_gettime(CLOCK_REALTIME, timespec);
    //TODO: include a default message ?
	*value = std::string("");
}
