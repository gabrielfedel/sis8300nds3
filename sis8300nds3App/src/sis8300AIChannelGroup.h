/**
 * @file sis8300AIChannelGroup.h
 * @brief Header file defining the analog input channel group class.
 * @author gabrielfedel
 * @date 2020-12-23
 *
 *
 */

#ifndef SIS8300AICHANNELGROUP_H
#define SIS8300AICHANNELGROUP_H

#include <nds3/nds.h>
#include <sis8300drv.h>

#include "sis8300.h"
#include "sis8300AIChannel.h"

/**
 * @brief sis8300AIChannelGroup
 */

long elapsed_time(timespec start, timespec end);


class sis8300AIChannelGroup {
public:
    sis8300AIChannelGroup(const std::string& name, nds::Node& parentNode, sis8300drv_usr  *deviceUser);

    nds::Port m_node;
    nds::StateMachine m_stateMachine;
    
    uint32_t m_numChannels;
    
    /* Class parameter that holds analog channels objects */
    std::vector<std::shared_ptr<sis8300AIChannel> > m_AIChannels;

    //void markAllParametersChanged();

    void getInfoMessage(timespec *timespec, std::string *value);
//    void getTriggerButton(timespec *timespec, std::string *value);

    void setSamples(const timespec &timespec, const int32_t &value);
    void getSamples(timespec *timespec, int32_t *value);

//    void setSampleRate(const timespec &timespec, const double &value);
//    void getSampleRate(timespec *timespec, double *value);
//
//    void getTriggerThreshold(timespec* timespec, double* value);
//    void setTriggerThreshold(const timespec &timespec, const double &value);
//
//    void getTriggerEdge(timespec* timespec, int32_t* value);
//    void setTriggerEdge(const timespec &timespec, const int32_t &value);
//
//    void getTriggerRepeat(timespec* timespec, int32_t* value);
//    void setTriggerRepeat(const timespec &timespec, const int32_t &value);
//
//    void getTriggerSource(timespec* timespec, int32_t* value);
//    void setTriggerSource(const timespec &timespec, const int32_t &value);
//
//    void getTriggerDelay(timespec* timespec, int32_t* value);
//    void setTriggerDelay(const timespec &timespec, const int32_t &value);
//
//    void getClockSource(timespec* timespec, int32_t* value);
//    void setClockSource(const timespec &timespec, const int32_t &value);
//
//    void getClockFrequency(timespec* timespec, double* value);
//    void setClockFrequency(const timespec &timespec, const double &value);
//
//    void getClockDivisor(timespec* timespec, int32_t* value);
//    void setClockDivisor(const timespec &timespec, const int32_t &value);
//
//    void getDecimation(timespec* timespec, int32_t* value);
//    void setDecimation(const timespec &timespec, const int32_t &value);
//
//    void getAveraging(timespec* timespec, int32_t* value);
//    void setAveraging(const timespec &timespec, const int32_t &value);

    void onSwitchOn();
    void onSwitchOff();
    void onStart();
    void onStop();
    void recover();
    bool allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal);
    void commitParameters(bool calledFromAcquisitionThread = false);
    void commitSimTrigParameters();

private:
    sis8300drv_usr  *m_deviceUser;
    int32_t *m_rawData;
    int32_t m_nSamples;

//    double  m_sampleRate;
//
//    double  m_triggerThreshold;
//    int32_t m_triggerEdge;
//    int32_t m_triggerRepeat;
//    int32_t m_triggerSource;
//    int32_t m_triggerDelay;
//
//    int32_t m_clockSource;
//    double  m_clockFrequency;
//    int32_t m_clockDivisor;
//
//    int32_t m_decimation;
//    int32_t m_averaging;

    bool m_nSamplesChanged;
//    bool m_sampleRateChanged;
//
//    bool m_triggerThresholdChanged;
//    bool m_triggerEdgeChanged;
//    bool m_triggerRepeatChanged;
//    bool m_triggerSourceChanged;
//    bool m_triggerDelayChanged;
//
//    bool m_clockSourceChanged;
//    bool m_clockFrequencyChanged;
//    bool m_clockDivisorChanged;
//
//    bool m_decimationChanged;
//    bool m_averagingChanged;

    nds::PVDelegateIn<std::int32_t> m_nSamplesPV;
    nds::PVDelegateIn<std::string> m_infoPV;
//    nds::PVDelegateIn<double>      m_sampleRatePV;
//
//    nds::PVDelegateIn<double>       m_triggerThresholdPV;
//    nds::PVDelegateIn<std::int32_t> m_triggerEdgePV;
//    nds::PVDelegateIn<std::int32_t> m_triggerRepeatPV;
//    nds::PVDelegateIn<std::int32_t> m_triggerSourcePV;
//    nds::PVDelegateIn<std::int32_t> m_triggerDelayPV;
//    nds::PVVariableIn<std::int32_t> m_triggerCounterPV;
//
//    nds::PVDelegateIn<std::int32_t> m_clockSourcePV;
//    nds::PVDelegateIn<double> m_clockFrequencyPV;
//    nds::PVDelegateIn<std::int32_t> m_clockDivisorPV;
//
//    nds::PVDelegateIn<std::int32_t> m_decimationPV;
//    nds::PVDelegateIn<std::int32_t> m_averagingPV;

    nds::Thread m_acquisitionThread;

    void acquisitionLoop(int32_t repeat);
    bool m_stop;
};




#endif /* SIS8300AICHANNELGROUP_H */
