#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>
#include <unistd.h>

#include <nds3/nds.h>
//#include <ifcdaqdrv.h>

#include "sis8300AIChannelGroup.h"


sis8300AIChannelGroup::sis8300AIChannelGroup(const std::string& name, nds::Node& parentNode, sis8300drv_usr  *deviceUser) :
    m_node(nds::Port(name, nds::nodeType_t::generic)),
    m_deviceUser(deviceUser),
    m_nSamples(4*1024),
    m_nSamplesChanged(true),
    m_nSamplesPV(nds::PVDelegateIn<std::int32_t>("nSamples-RB",
                                                   std::bind(&sis8300AIChannelGroup::getSamples,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_infoPV(nds::PVDelegateIn<std::string>("InfoMessage",                                                      
                                                    std::bind(&sis8300AIChannelGroup::getInfoMessage, 
                                                    this,
                                                    std::placeholders::_1,
                                                    std::placeholders::_2)))

{
    parentNode.addChild(m_node);

    m_numChannels = SIS8300DRV_NUM_AI_CHANNELS;

    m_rawData = (int32_t *)calloc(m_numChannels * CHANNEL_SAMPLES_MAX, sizeof(int32_t));
    if(!m_rawData)
    {
        throw nds::NdsError("Out of memory");
    }

    for(size_t numChannel(0); numChannel != m_numChannels; ++numChannel)
    {
        std::ostringstream channelName;
        channelName << "CH" << numChannel;
        m_AIChannels.push_back(std::make_shared<sis8300AIChannel>(channelName.str(), m_node, numChannel, m_deviceUser, m_rawData, 10.0/SHRT_MAX));
    }

    // PVs for Number of Samples.
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("nSamples",
                                                   std::bind(&sis8300AIChannelGroup::setSamples,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&sis8300AIChannelGroup::getSamples,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_nSamplesPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_nSamplesPV);

//    // PVs for Sample rate
//    m_node.addChild(nds::PVDelegateOut<double>("SampleRate",
//                                                   std::bind(&sis8300AIChannelGroup::setSampleRate,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getSampleRate,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_sampleRatePV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_sampleRatePV);
//
//    // PVs for Trigger Source
//    nds::enumerationStrings_t triggerSourceStrings;
//    triggerSourceStrings.push_back("Software");
//    triggerSourceStrings.push_back("FP-GPIO");
//    triggerSourceStrings.push_back("AMC_RX_17");
//    triggerSourceStrings.push_back("AMC_RX_18");
//    triggerSourceStrings.push_back("AMC_RX_19");
//    triggerSourceStrings.push_back("AMC_RX_20");
//    triggerSourceStrings.push_back("CH-0");
//    triggerSourceStrings.push_back("CH-1");
//    triggerSourceStrings.push_back("CH-2");
//    triggerSourceStrings.push_back("CH-3");
//    triggerSourceStrings.push_back("CH-4");
//    triggerSourceStrings.push_back("CH-5");
//    triggerSourceStrings.push_back("CH-6");
//    triggerSourceStrings.push_back("CH-7");
//
//    
//    nds::PVDelegateOut<std::int32_t> node(nds::PVDelegateOut<std::int32_t>("TriggerSource",
//                                                   std::bind(&sis8300AIChannelGroup::setTriggerSource,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getTriggerSource,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//    node.setEnumeration(triggerSourceStrings);
//    m_node.addChild(node);
//
//    m_triggerSourcePV.setScanType(nds::scanType_t::interrupt);
//    m_triggerSourcePV.setEnumeration(triggerSourceStrings);
//    m_node.addChild(m_triggerSourcePV);
//
//    //PVs for Trigger Threshold
//    m_node.addChild(nds::PVDelegateOut<double>("TriggerThreshold",
//                                                   std::bind(&sis8300AIChannelGroup::setTriggerThreshold,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getTriggerThreshold,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_triggerThresholdPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_triggerThresholdPV);
//
//    //PVs for Trigger Edge
//    nds::enumerationStrings_t triggerEdgeStrings;
//    triggerEdgeStrings.push_back("Rising Edge");
//    triggerEdgeStrings.push_back("Falling Edge");
//    node = nds::PVDelegateOut<std::int32_t>("TriggerEdge",
//                                            std::bind(&sis8300AIChannelGroup::setTriggerEdge,
//                                                      this,
//                                                      std::placeholders::_1,
//                                                      std::placeholders::_2),
//                                            std::bind(&sis8300AIChannelGroup::getTriggerEdge,
//                                                      this,
//                                                      std::placeholders::_1,
//                                                      std::placeholders::_2));
//    node.setEnumeration(triggerEdgeStrings);
//    m_node.addChild(node);
//
//    m_triggerEdgePV.setScanType(nds::scanType_t::interrupt);
//    m_triggerEdgePV.setEnumeration(triggerEdgeStrings);
//    m_node.addChild(m_triggerEdgePV);
//
//    // PVs for Trigger Repeat
//    m_node.addChild(nds::PVDelegateOut<std::int32_t>("TriggerRepeat",
//                                                   std::bind(&sis8300AIChannelGroup::setTriggerRepeat,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getTriggerRepeat,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_triggerRepeatPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_triggerRepeatPV);
//
//    // PVs for Trigger delay
//    m_node.addChild(nds::PVDelegateOut<std::int32_t>("TriggerDelay",
//                                                   std::bind(&sis8300AIChannelGroup::setTriggerDelay,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getTriggerDelay,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_triggerDelayPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_triggerDelayPV);
//
//    // PV for Trigger Counter
//    m_triggerCounterPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_triggerCounterPV);
//
//    // PVs for Clock Source
//    nds::enumerationStrings_t clockSourceStrings;
//    clockSourceStrings.push_back("Internal");
//    clockSourceStrings.push_back("External");
//    node = nds::PVDelegateOut<std::int32_t>("ClockSource",
//                                                   std::bind(&sis8300AIChannelGroup::setClockSource,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getClockSource,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2));
//    node.setEnumeration(clockSourceStrings);
//    m_node.addChild(node);

//    m_clockSourcePV.setScanType(nds::scanType_t::interrupt);
//    m_clockSourcePV.setEnumeration(clockSourceStrings);
//    m_node.addChild(m_clockSourcePV);
//
//    // PVs for Clock Frequency
//    m_node.addChild(nds::PVDelegateOut<double>("ClockFrequency",
//                                                   std::bind(&sis8300AIChannelGroup::setClockFrequency,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getClockFrequency,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_clockFrequencyPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_clockFrequencyPV);
//
//
//    // PVs for Clock Divisor
//    m_node.addChild(nds::PVDelegateOut<std::int32_t>("ClockDivisor",
//                                                   std::bind(&sis8300AIChannelGroup::setClockDivisor,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getClockDivisor,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_clockDivisorPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_clockDivisorPV);
//
//    // PVs for Hardware Decimation
//    m_node.addChild(nds::PVDelegateOut<std::int32_t>("Decimation",
//                                                   std::bind(&sis8300AIChannelGroup::setDecimation,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getDecimation,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_decimationPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_decimationPV);
//
//    // PVs for Hardware averaging
//    m_node.addChild(nds::PVDelegateOut<std::int32_t>("Averaging",
//                                                   std::bind(&sis8300AIChannelGroup::setAveraging,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2),
//                                                   std::bind(&sis8300AIChannelGroup::getAveraging,
//                                                             this,
//                                                             std::placeholders::_1,
//                                                             std::placeholders::_2)));
//
//    m_averagingPV.setScanType(nds::scanType_t::interrupt);
//    m_node.addChild(m_averagingPV);

    // PV for debug/info messages.
    m_infoPV.setScanType(nds::scanType_t::interrupt);
    m_infoPV.setMaxElements(512);
    m_node.addChild(m_infoPV);

    // PVs for state machine.
    m_stateMachine = m_node.addChild(nds::StateMachine(true,
                                    std::bind(&sis8300AIChannelGroup::onSwitchOn, this),
                                    std::bind(&sis8300AIChannelGroup::onSwitchOff, this),
                                    std::bind(&sis8300AIChannelGroup::onStart, this),
                                    std::bind(&sis8300AIChannelGroup::onStop, this),
                                    std::bind(&sis8300AIChannelGroup::recover, this),
                                    std::bind(&sis8300AIChannelGroup::allowChange,
                                              this,
                                              std::placeholders::_1,
                                              std::placeholders::_2,
                                              std::placeholders::_3)));

    // Initialize some values from hardware
//    ifcdaqdrv_get_clock_source(&deviceUser, (ifcdaqdrv_clock *)&m_clockSource);
//    ifcdaqdrv_get_clock_frequency(&deviceUser, &m_clockFrequency);
//    ifcdaqdrv_get_clock_divisor(&deviceUser, (uint32_t *)&m_clockDivisor);
//    ifcdaqdrv_get_sample_rate(&deviceUser, &m_sampleRate);
    commitParameters();

}

///*
// * Round upwards to nearest power-of-2.
// */
//
//static inline int ceil_pow2(unsigned number) {
//    unsigned n = 1;
//    unsigned i = number - 1;
//    while(i) {
//        n <<= 1;
//        i >>= 1;
//    }
//    return n;
//}
//
///*
// * Round upwards to nearest mibibytes
// */
//
//static inline int ceil_mibi(unsigned number) {
//    return (1 + (number-1) / (1024 * 1024)) * 1024 * 1024;
//}

//void sis8300AIChannelGroup::getTriggerButton(timespec* timespec, std::string *value)
//{
//}

void sis8300AIChannelGroup::getInfoMessage(timespec* timespec, std::string *value)
{
//    std::ostringstream tmp;
//    char manufacturer[100];
//    ifcdaqdrv_get_manufacturer(&m_deviceUser, manufacturer, 100);
//    char product_name[100];
//    ifcdaqdrv_get_product_name(&m_deviceUser, product_name, 100);
//    tmp << manufacturer << " " << product_name;
//    *value = tmp.str();
}

void sis8300AIChannelGroup::getSamples(timespec* timespec, int32_t* value)
{
    *value = m_nSamples;
}

void sis8300AIChannelGroup::setSamples(const timespec& timespec, const int32_t& value)
{
    int32_t  samplesCountMax;

    ndsInfoStream << __func__ << endl;
    ndsDebugStream << "Requested sample number = " << value << endl;

	// Reads from RAM should be aligned by offsets of 32.
	value = ceil_pow2((unsigned)value);	
	ndsDebugStream << "Requested sample number rounded up to %d for memory alignment reasons." << value;

    if (value < 0) {
        SIS8300NDS_STATUS_MSGERR(__func__, "Number of samples cannot be negative.")
        value = 0;
    }

    //TODO : review if this will be necessary
//    /* Only negative trigger delays are supported (samples before trigger). */
//    if (value < -_TriggerDelay) {
//        SIS8300NDS_STATUS_MSGERR(__func__, "Number of samples cannot be less then absolute value of trigger delay %d.", -_TriggerDelay);
//        value = -_TriggerDelay;
//    }

    //STOPPED HERE

    /* If the device is in a state with an open device descriptor check
     * if the setting is valid with respect to available device memory.
     * Check this with the current number of enabled channels.
     * If the desired number of samples is to high the value is clipped. */
    if (_device->getCurrentState() != nds::DEVICE_STATE_OFF &&
            _device->getCurrentState() != nds::DEVICE_STATE_IOC_INIT) {
        statusSamplesCount = checkSamplesConfig(value, -1, &samplesCountMax);
        if (statusSamplesCount != ndsSuccess) {
            value = samplesCountMax;
            status = ndsError;
        }
    }

    NDS_DBG("Setting number of samples to %d.", value);

    _SamplesCount = value;
    _SamplesCountChanged = 1;

    statusCommit = commitParameters();
    if (statusCommit != ndsSuccess) {
        status = ndsError;
    }
    return status;







//    if(value < -m_triggerDelay) {
//        sis8300NDS_MSGWRN("Number of samples has to fit all Trigger Delay samples");
//        return;
//    }
    if(value > CHANNEL_SAMPLES_MAX) {
        SIS8300NDS_MSGWRN("Buffer not large enough for samples. Dynamic allocation not implemented yet.");
        return;
    }
    m_nSamples = value;
    m_nSamplesChanged = true;
    commitParameters();
}

//void sis8300AIChannelGroup::getSampleRate(timespec* timespec, double* value) {
//    *value = m_sampleRate;
//}
//void sis8300AIChannelGroup::setSampleRate(const timespec& timespec, const double& value)
//{
//    if(value < 0) {
//        sis8300NDS_MSGWRN("Sample rate cannot be negative.");
//        return;
//    }
//
//    m_sampleRate = value;
//    m_sampleRateChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getTriggerSource(timespec* timespec, int32_t* value)
//{
//    *value = m_triggerSource;
//}
//
//void sis8300AIChannelGroup::setTriggerSource(const timespec& timespec, const int32_t& value)
//{
//    m_triggerSource = value;
//    m_triggerSourceChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getTriggerThreshold(timespec* timespec, double* value)
//{
//    *value = m_triggerThreshold;
//}
//
//void sis8300AIChannelGroup::setTriggerThreshold(const timespec& timespec, const double& value)
//{
//    m_triggerThreshold = value;
//    m_triggerThresholdChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getTriggerEdge(timespec* timespec, int32_t* value)
//{
//    *value = m_triggerEdge;
//}
//
//void sis8300AIChannelGroup::setTriggerEdge(const timespec& timespec, const int32_t& value)
//{
//    m_triggerEdge = value;
//    m_triggerEdgeChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getTriggerRepeat(timespec* timespec, int32_t* value)
//{
//    *value = m_triggerRepeat;
//}
//
//void sis8300AIChannelGroup::setTriggerRepeat(const timespec& timespec, const int32_t& value)
//{
//    if(value < -1 || value == 0) {
//        sis8300NDS_MSGWRN("Invalid Trigger Repeat value");
//        return;
//    }
//    m_triggerRepeat = value;
//    m_triggerRepeatChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getTriggerDelay(timespec* timespec, int32_t* value)
//{
//    *value = m_triggerDelay;
//}
//
//void sis8300AIChannelGroup::setTriggerDelay(const timespec& timespec, const int32_t& value)
//{
//    if(-value > m_nSamples){
//        sis8300NDS_MSGWRN("Trigger Delay cannot exceed number of samples");
//        return;
//    }
//    if(value > 0){
//        sis8300NDS_MSGWRN("Positive Trigger Delay is not supported.");
//        return;
//    }
//    m_triggerDelay = value;
//    m_triggerDelayChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getClockSource(timespec* timespec, int32_t* value)
//{
//    *value = m_clockSource;
//}
//
//void sis8300AIChannelGroup::setClockSource(const timespec& timespec, const int32_t& value)
//{
//    m_clockSource = value;
//    m_clockSourceChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getClockFrequency(timespec* timespec, double* value)
//{
//    *value = m_clockFrequency;
//}
//
//void sis8300AIChannelGroup::setClockFrequency(const timespec& timespec, const double& value)
//{
//    m_clockFrequency = value;
//    m_clockFrequencyChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getClockDivisor(timespec* timespec, int32_t* value)
//{
//    *value = m_clockDivisor;
//}
//
//void sis8300AIChannelGroup::setClockDivisor(const timespec& timespec, const int32_t& value)
//{
//    m_clockDivisor = value;
//    m_clockDivisorChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getDecimation(timespec* timespec, int32_t* value)
//{
//    *value = m_decimation;
//}
//
//void sis8300AIChannelGroup::setDecimation(const timespec& timespec, const int32_t& value)
//{
//    m_decimation = value;
//    m_decimationChanged = true;
//    commitParameters();
//}
//
//void sis8300AIChannelGroup::getAveraging(timespec* timespec, int32_t* value)
//{
//    *value = m_averaging;
//}
//
//void sis8300AIChannelGroup::setAveraging(const timespec& timespec, const int32_t& value)
//{
//    m_averaging = value;
//    m_averagingChanged = true;
//    commitParameters();
//}


/* commit parameters to hardware.
 *
 * the scope application supports
 */

void sis8300AIChannelGroup::commitParameters(bool calledFromAcquisitionThread)
{
    struct timespec now = {0, 0};
//    ifcdaqdrv_status status;

    clock_gettime(CLOCK_REALTIME, &now);

    /* If the call comes from the acquisition thread it is certain that the device
     * is not armed. Otherwise, only allow changes if device is in on or going to on. */

    if(!calledFromAcquisitionThread && (
            m_stateMachine.getLocalState() != nds::state_t::on &&
            m_stateMachine.getLocalState() != nds::state_t::stopping  &&
            m_stateMachine.getLocalState() != nds::state_t::initializing )) {
        return;
    }

    /*
     * The board has a circular buffer so that values before the trigger can be read out. This is specified as "number of
     * pre-trigger samples". The amount has to be an even eighth of the total number of samples and it cannot be all
     * samples.
     *
     * The user should be able to enter an arbitrary amount of samples and pre-trigger samples. Using the following
     * "algorithm" the device support will then try to find a valid configuration to get the requested values.
     *
     * 1. First we calculate an intial pre-trigger samples quotient (ptq) to estimate this eighth.
     * 2. If ptq is 8 (8 eighths are requested as pre trigger samples) then the amount of samples are doubled so that
     *    ptq is closer to 4 eighths.
     * 3. If we then cannot satisfy the amount of post trigger samples, the number of samples has to be doubled again.
     *
     * Obvioysly pre-trigger samples are not allowed to be larger than the total amount of samples. This is enforced
     * in the setters earlier.
     */

    uint32_t nchannels;
    uint32_t sample_size;
    //TODO: sample size and nchannels
    nchannels = 10;
    sample_size = 1024;

//    ifcdaqdrv_get_nchannels(&m_deviceUser, &nchannels);
//    ifcdaqdrv_get_resolution(&m_deviceUser, &sample_size);
    sample_size /= 8;

    if(m_nSamplesChanged) {
        m_nSamplesChanged = false;

        uint32_t nSamples = 0;
        if(m_nSamples <= 32*1024) {
            nSamples = ceil_pow2(m_nSamples);
        } else {
            // 1MiB gets us 65k samples (divide by nchannels and sample_size)
            nSamples = ceil_mibi(m_nSamples * nchannels * sample_size) / nchannels / sample_size;
        }
        uint32_t nPretrig = 0;
        
        /* Disable trigger delay when trigger source is SOFTWARE */
        // if (m_clockSource == 0)
        //     m_triggerDelay = 0;

//        if(m_triggerDelay < 0) {
//            nPretrig = -m_triggerDelay;
//        }

        /* Calculate initial ptq, ceil(8*(npretrig/nsamples)) */
        uint32_t ptq = (8 * nPretrig + nSamples - 1) / nSamples;

        /* The only valid values for ptq is 0..7. Double the number of samples to fit the requested number of
         * pre-trigger samples. */
        if(ptq >= 8) {
            ndsDebugStream(m_node) << "Doubling nsamples" << std::endl;
            nSamples *= 2;
            if(nSamples > 16*1024) {
                nSamples = ceil_mibi(nSamples * nchannels * sample_size) / nchannels / sample_size;
            }
            ptq = (8 * nPretrig + nSamples - 1) / nSamples;
        }

        /* Check that we get enough post-trigger samples. Double the number of samples to get the requested number of
         * post-trigger samples. */
        if((8-ptq) * nSamples / 8 < m_nSamples - nPretrig) {
            ndsDebugStream(m_node) << "Doubling nsamples" << std::endl;
            nSamples *= 2;
            if(nSamples > 16*1024) {
                nSamples = ceil_mibi(nSamples * nchannels * sample_size) / nchannels / sample_size;
            }
            ptq = (8 * nPretrig + nSamples - 1) / nSamples;
        }

        nPretrig = (nSamples * ptq) / 8;

        ndsDebugStream(m_node) << "Number of samples: " << nSamples << ", Number of pre-trigger samples: " << nPretrig << std::endl;

        //TODO: get number of samples
        nSamples = 1000;
//        status = ifcdaqdrv_set_nsamples(&m_deviceUser, nSamples);
//        sis8300NDS_STATUS_CHECK("set_nsamples", status);
//        if(status != status_success){
//            if (status == status_no_support){ /* SCOPE LITE is fixed 2048 samples */
//                status = ifcdaqdrv_get_nsamples(&m_deviceUser, (uint32_t *)&m_nSamples);
//                m_nSamplesPV.push(now, m_nSamples);
//                return;
//            }
//            std::ostringstream tmp;
//            tmp << "To many samples requested, tried " << nSamples << " samples with " << nPretrig << " pre-trigger samples.";
//            sis8300NDS_MSGWRN(tmp.str());
//            /* Set the largets possible amount of samples. */
//            do {
//                nSamples >>= 1;
//                status = ifcdaqdrv_set_nsamples(&m_deviceUser, nSamples);
//            } while(status && nSamples>0);
//            ifcdaqdrv_get_nsamples(&m_deviceUser, (uint32_t *)&m_nSamples);
//            nPretrig = m_triggerDelay = 0;
//        }

        m_nSamplesPV.push(now, m_nSamples);

//        status = ifcdaqdrv_set_npretrig(&m_deviceUser, nPretrig);
//        sis8300NDS_STATUS_CHECK("set_npretrig", status);
//        if(status) {
//            status = ifcdaqdrv_set_npretrig(&m_deviceUser, 0);
//            m_triggerDelay = 0;
//            m_triggerDelayPV.push(now, 0);
//        } else {
//            m_triggerDelayPV.push(now, m_triggerDelay);
//        }

    }


//    if(m_triggerSourceChanged || m_triggerEdgeChanged || m_triggerThresholdChanged) {
//        m_triggerThresholdChanged = false;
//        m_triggerSourceChanged = false;
//        m_triggerEdgeChanged = false;
//
//        ifcdaqdrv_trigger_type triggerType = ifcdaqdrv_trigger_none;
//        int32_t threshold = 0;
//        uint32_t mask = 0;
//        uint32_t polarity = 0;
//
//        // switch(m_triggerSource) {
//        // case 0:
//        // case 1:
//        // case 2:
//        // case 3:
//        // case 4:
//        // case 5:
//        // case 6:
//        // case 7:
//        //     triggerType = ifcdaqdrv_trigger_frontpanel;
//        //     mask = 1 << m_triggerSource;
//        //     polarity = ((m_triggerEdge + 1) % 2) << m_triggerSource;
//        //     struct timespec tmp_time;
//        //     double linConvFactor;
//        //     try {
//        //         m_AIChannels.at(m_triggerSource)->getLinearConversionFactor(&tmp_time, &linConvFactor);
//        //         threshold = m_triggerThreshold/linConvFactor;
//        //     } catch (const std::out_of_range& oor) {
//        //         // Nothing to do if channel doesn't exist.
//        //     }
//        //     break;
//        // case 8:
//        //     triggerType = ifcdaqdrv_trigger_frontpanel;
//        //     mask = 1 << 30;
//        //     polarity = ((m_triggerEdge + 1) % 2) << 30;
//        //     break;
//        // case 9:
//        //     triggerType = ifcdaqdrv_trigger_backplane;
//        //     break;
//        // case 10:
//        //     triggerType = ifcdaqdrv_trigger_soft;
//        //     break;
//        // default:
//        //     triggerType = ifcdaqdrv_trigger_none;
//        // }
//
//        switch (m_triggerSource)
//        {
//            case 0: // Software
//                triggerType = ifcdaqdrv_trigger_soft;
//                break;
//            // case 1: // MicroTCA Backplane
//            //     triggerType = ifcdaqdrv_trigger_backplane;
//            //     break;
//            case 1: // Frontpanel GPIO
//                triggerType = ifcdaqdrv_trigger_frontpanel;
//                mask = 1 << 30;
//                polarity = ((m_triggerEdge + 1) % 2) << 30;
//                break;
//            
//            /* From 2 to 5 is MTCA backplane trigger */    
//            case 2:
//            case 3:
//            case 4:
//            case 5:
//                /* Use variable mask to specify backplane line */
//                mask = m_triggerSource - 2; // 0 to 7 
//                triggerType = ifcdaqdrv_trigger_backplane;
//                break;
//
//            /* From 6 to 13 is ADC input as trigger */    
//            case 6:
//            case 7:
//            case 8:
//            case 9:
//            case 10:
//            case 11:
//            case 12:
//            case 13:
//                triggerType = ifcdaqdrv_trigger_frontpanel;
//                mask = 1 << (m_triggerSource - 6);
//                polarity = ((m_triggerEdge + 1) % 2) << (m_triggerSource - 6);
//                struct timespec tmp_time;
//                double linConvFactor;
//                try {
//                    m_AIChannels.at(m_triggerSource-6)->getLinearConversionFactor(&tmp_time, &linConvFactor);
//                    threshold = m_triggerThreshold/linConvFactor;
//                } catch (const std::out_of_range& oor) {
//                    // Nothing to do if channel doesn't exist.
//                    triggerType = ifcdaqdrv_trigger_soft;
//                }
//                break;
//
//            default:
//                triggerType = ifcdaqdrv_trigger_soft;
//        }
//
//        status = ifcdaqdrv_set_trigger(&m_deviceUser, triggerType, threshold, mask, polarity);
//        sis8300NDS_STATUS_CHECK("set_trigger", status);
//        if(!status) {
//            m_triggerThresholdPV.push(now, m_triggerThreshold);
//            m_triggerSourcePV.push(now, m_triggerSource);
//            m_triggerEdgePV.push(now, m_triggerEdge);
//        }
//    }
//
//    if(m_triggerRepeatChanged && m_stateMachine.getLocalState() != nds::state_t::running) {
//        m_triggerRepeatChanged = false;
//        m_triggerRepeatPV.push(now, m_triggerRepeat);
//    }
//
//    if(m_clockSourceChanged) {
//        m_clockSourceChanged = false;
//        status = ifcdaqdrv_set_clock_source(&m_deviceUser, (ifcdaqdrv_clock)m_clockSource);
//        sis8300NDS_STATUS_CHECK("set_clock_source", status);
//        if(!status) {
//            m_clockSourcePV.push(now, m_clockSource);
//        }
//    }
//
//    if(m_clockFrequencyChanged) {
//        m_clockFrequencyChanged = false;
//        status = ifcdaqdrv_set_clock_frequency(&m_deviceUser, m_clockFrequency);
//        sis8300NDS_STATUS_CHECK("set_clock_frequency", status);
//        if(!status) {
//            m_clockFrequencyPV.push(now, m_clockFrequency);
//            ifcdaqdrv_calc_sample_rate(&m_deviceUser, &m_averaging, &m_decimation, &m_clockDivisor, &m_clockFrequency, &m_sampleRate, 0);
//            m_sampleRatePV.push(now, m_sampleRate);
//        }
//    }
//
//    if(m_clockDivisorChanged) {
//        m_clockDivisorChanged = false;
//        status = ifcdaqdrv_set_clock_divisor(&m_deviceUser, m_clockDivisor);
//        sis8300NDS_STATUS_CHECK("set_clock_divisor", status);
//        if(!status) {
//            m_clockDivisorPV.push(now, m_clockDivisor);
//            ifcdaqdrv_calc_sample_rate(&m_deviceUser, &m_averaging, &m_decimation, &m_clockDivisor, &m_clockFrequency, &m_sampleRate, 0);
//            m_sampleRatePV.push(now, m_sampleRate);
//        }
//    }
//
//    if(m_decimationChanged) {
//        m_decimationChanged = false;
//        status = ifcdaqdrv_set_decimation(&m_deviceUser, m_decimation);
//        sis8300NDS_STATUS_CHECK("set_decimation", status);
//        if(!status) {
//            m_decimationPV.push(now, m_decimation);
//            ifcdaqdrv_calc_sample_rate(&m_deviceUser, &m_averaging, &m_decimation, &m_clockDivisor, &m_clockFrequency, &m_sampleRate, 0);
//            m_sampleRatePV.push(now, m_sampleRate);
//        }
//    }
//
//    if(m_averagingChanged) {
//        m_averagingChanged = false;
//        status = ifcdaqdrv_set_average(&m_deviceUser, m_averaging);
//        sis8300NDS_STATUS_CHECK("set_average", status);
//        if(!status) {
//            m_averagingPV.push(now, m_averaging);
//            ifcdaqdrv_calc_sample_rate(&m_deviceUser, &m_averaging, &m_decimation, &m_clockDivisor, &m_clockFrequency, &m_sampleRate, 0);
//            m_sampleRatePV.push(now, m_sampleRate);
//        }
//    }
//
//    if(m_sampleRateChanged) {
//        m_sampleRateChanged = false;
//
//        ifcdaqdrv_calc_sample_rate(&m_deviceUser, &m_averaging, &m_decimation, &m_clockDivisor, &m_clockFrequency, &m_sampleRate, 1);
//
//        m_sampleRatePV.push(now, m_sampleRate);
//        m_clockFrequencyPV.push(now, m_clockFrequency);
//        m_clockDivisorPV.push(now, m_clockDivisor);
//        m_averagingPV.push(now, m_averaging);
//        m_decimationPV.push(now, m_decimation);
//    }

//    ifcdaqdrv_send_configuration_command(&m_deviceUser);
}

void sis8300AIChannelGroup::onSwitchOn()
{
    // Enable all channels
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::on);
    }
    commitParameters();
}

void sis8300AIChannelGroup::onSwitchOff()
{
    // Disable all channels
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::off);
    }
}

void sis8300AIChannelGroup::onStart()
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);

    if(!m_nSamples) {
        m_infoPV.push(now, std::string("Samples is 0, won't start."));
        throw nds::StateMachineRollBack("Samples is 0, won't start.");
    }

//    if(!m_triggerRepeat) {
//        m_infoPV.push(now, std::string("Trigger Repeat is 0, won't start."));
//        throw nds::StateMachineRollBack("Trigger Repeat is 0, won't start.");
//    }

    /* Start all Channels */
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::running);
    }

    // Start data acquisition
    m_stop = false;
    //TODO: review the last parameter in the bind
    m_acquisitionThread = m_node.runInThread("AcquisitionLoop", std::bind(&sis8300AIChannelGroup::acquisitionLoop, this, true));
}

void sis8300AIChannelGroup::onStop()
{
    m_stop = true;
    //TODO: disarm ?
//    ifcdaqdrv_disarm_device(&m_deviceUser);

    m_acquisitionThread.join();
    // Stop channels
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::on);
    }

    commitParameters();
}

void sis8300AIChannelGroup::recover()
{
    throw nds::StateMachineRollBack("Cannot recover");
}

bool sis8300AIChannelGroup::allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal)
{
    return true;
}

void sis8300AIChannelGroup::acquisitionLoop(int32_t repeat)
{

//    ndsInfoStream(m_node) << "Acquisition started, iterations: " << repeat << std::endl;
//    struct timespec now;
//    struct timespec ts_start, ts_end;
//    struct timespec ts_iterstart, ts_iterend;
//    long __attribute__((unused)) measured_tspec; // just for when one wants to debug...
//    ifcdaqdrv_status status;
//    uint32_t nSamplesHw = 0;
//    int32_t nSamples = 0;
//    uint32_t nPretrigHw = 0;
//    int32_t nPretrig = 0;
//
//    clock_gettime(CLOCK_REALTIME, &now);
//    m_triggerCounterPV.push(now, 0);
//
//    sis8300NDS_MSGWRN("Acquisition running...");
//
//    for(int32_t i = 0; repeat < 0 || i != repeat; ++i)
//    {
//        ifcdaqdrv_get_nsamples(&m_deviceUser, &nSamplesHw);
//        ifcdaqdrv_get_npretrig(&m_deviceUser, &nPretrigHw);
//        nSamples = m_nSamples < (int32_t)nSamplesHw ? m_nSamples : nSamplesHw;
//        nPretrig = -m_triggerDelay;
//        if(m_stop) {
//            break;
//        }
//        
//
//        clock_gettime(CLOCK_REALTIME, &ts_start);
//        ts_iterstart = ts_start;
//
//        status = ifcdaqdrv_arm_device(&m_deviceUser);
//
//        /* Silent exit the loop when the BUG of the first acquisition happens */
//        // if (status == -24)
//        //     break;
//
//        clock_gettime(CLOCK_REALTIME, &ts_end);
//        measured_tspec = elapsed_time(ts_start, ts_end) / 1000;
//        //std::cout << "[ifcdaqdrv] ifcdaqdrv_arm_device took " << measured_tspec << " us" << std::endl;
//
//        sis8300NDS_STATUS_CHECK("arm_device", status);
//        ndsDebugStream(m_node) << "Number of samples in HW: " << nSamplesHw << std::endl;
//
//
//        clock_gettime(CLOCK_REALTIME, &ts_start);
//
//        status = ifcdaqdrv_wait_acq_end(&m_deviceUser);
//        if(status == status_cancel) {
//            ndsInfoStream(m_node) << "Acquisition cancelled." << std::endl;
//            break;
//        }
//        else if(status != status_success) {
//            m_stateMachine.setState(nds::state_t::fault);
//            break;
//        }
//
//        clock_gettime(CLOCK_REALTIME, &ts_end);
//        measured_tspec = elapsed_time(ts_start, ts_end) / 1000;
//        //std::cout << "[ifcdaqdrv] ifcdaqdrv_wait_acq_end took " << measured_tspec << " us" << std::endl;
//
//
//
//        m_triggerCounterPV.push(now, i + 1);
//
//        clock_gettime(CLOCK_REALTIME, &ts_start);
//        
//        ifcdaqdrv_read_ai(&m_deviceUser, m_rawData);
//        
//        clock_gettime(CLOCK_REALTIME, &ts_end);
//        measured_tspec = elapsed_time(ts_start, ts_end) / 1000;
//        //std::cout << "[ifcdaqdrv] ifcdaqdrv_read_ai took " << measured_tspec << " us" << std::endl;
//        
//        clock_gettime(CLOCK_REALTIME, &ts_start);
//        for(auto const& channel: m_AIChannels) {
//            //std::cout << "read ch" << channel->m_channelNum << std::endl;
//            //ifcdaqdrv_read_ai_ch(&m_deviceUser, channel->m_channelNum, m_rawData + channel->m_channelNum * CHANNEL_SAMPLES_MAX);
//            
//            channel->read(m_rawData + (channel->m_channelNum * nSamplesHw), nSamples, nSamplesHw, nPretrig, nPretrigHw);
//            /* Copy data */
//            //std::cerr << channel << std::endl;
//        }
//
//        clock_gettime(CLOCK_REALTIME, &ts_end);
//        measured_tspec = elapsed_time(ts_start, ts_end) / 1000;
//        //std::cout << "[ifcdaq] reading all channels took " << measured_tspec << " us" << std::endl;
//
//        commitParameters(true);
//        clock_gettime(CLOCK_REALTIME, &ts_end);
//        ts_iterend = ts_end;
//    }
//
//    ndsInfoStream(m_node) << "Acquisition finished." << std::endl;
//    sis8300NDS_MSGWRN("Acquisition finished");
//
//    try {
//        m_stateMachine.setState(nds::state_t::on);
//    } catch (nds::StateMachineNoSuchTransition &) {
//        /* We are probably already in "stopping", no need to panic... */
//    }
//
}

long elapsed_time(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp.tv_nsec;
}    
