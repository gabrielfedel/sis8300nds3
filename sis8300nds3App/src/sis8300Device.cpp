#include "sis8300Device.h"


// The implementation of this class is based on the NDS2 version of sis8300 
// And sis8300llrf. For now this version will include what is define on the
// sis8300 from the FIM. In the future we can consider having
// it in a new class
sis8300Device::sis8300Device(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters) :
        m_node(deviceName)
{
//    ifcdaqdrv_status status;
//
//    m_deviceUser = {static_cast<uint32_t>(std::stoul(parameters.at("card"))),
//                    static_cast<uint32_t>(std::stoul(parameters.at("fmc"))),
//                    NULL};
//
//    status = ifcdaqdrv_open_device(&m_deviceUser);
//    if(status != status_success) {
//        throw nds::NdsError("Failed to open device");
//    }
//    status = ifcdaqdrv_init_adc(&m_deviceUser);
//    if(status != status_success) {
//        throw nds::NdsError("Failed to initialize ADC");
//    }
//
    
    m_deviceUser = new sis8300drv_usr();
    //m_deviceUser->file = getStrParam("FILE", "").c_str();
    //TODO: check if FILE exists before get it
	if (parameters.find("file") == parameters.end()) {
        std::cout << "card not found!";
	} else {
        m_deviceUser->file = parameters.at("file").c_str();
	}

    std::shared_ptr<sis8300AIChannelGroup> aichgrp = std::make_shared<sis8300AIChannelGroup>("AI", m_node, m_deviceUser);
    m_AIChannelGroup.push_back(aichgrp);

    m_FSM = std::make_shared<sis8300FSM>("FSM", m_node, *m_deviceUser);

    // Initialize device
    m_node.initialize(this, factory);
    std::cout << "[LLRFIOC] SIS8300 is initialized" << std::endl;

    //Move state machine to ON
    m_FSM->m_stateMachine.setState(nds::state_t::on);


}

sis8300Device::~sis8300Device() {
    std::cout << "[LLRF] Gracefully exiting IOC..." << std::endl;
    sis8300drv_close_device(m_deviceUser);
}

//NDS_DEFINE_DRIVER(sis8300, sis8300Device);

extern "C" { 

void* allocateDevice(nds::Factory& factory, const std::string& device, const nds::namedParameters_t& parameters) { 
	return new sis8300Device(factory, device, parameters); 
} 

void deallocateDevice(void* device) 
{ 
	delete (sis8300Device*)device; 
} 

const char* getDeviceName() { 
	return "sis8300"; 
} 

nds::RegisterDevice<sis8300Device> registerDevicesis8300("sis8300"); 


/**
 * @brief Callback function for EPICS IOC initialization steps.  Used to trigger
 *        normal processing by the driver.
 *
 * @param[in] state IOC current state
 */
void sis8300_initHookFunc(initHookState state)
{

}

/**
 * @brief The function that registers the EPIS IOC Shell functions
 */
void sis8300_Register(void)
{
  static bool firstTime = true;

  if (firstTime)
  {
    initHookRegister(sis8300_initHookFunc);
  }
}

epicsExportRegistrar(sis8300_Register);



}
