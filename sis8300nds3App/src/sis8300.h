#ifndef SIS8300_H
#define SIS8300_H


#include <iostream>
#include <sstream>

/*
 * Helper macro to push text to user through the INFO pv
 *
 * @param text Has to be something that can be passed to std::string()
 */

//TODO: define a m_infoPV ?
#define SIS8300NDS_MSGWRN(text)                         \
    do {                                               \
        struct timespec now;                           \
        clock_gettime(CLOCK_REALTIME, &now);           \
        m_infoPV.push(now, std::string(text));                    \
        ndsWarningStream(m_node) << std::string(text) << "\n";    \
    } while(0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define SIS8300NDS_STATUS_MSGERR(func, status)                          \
    do {                                                                \
        struct timespec now;                                            \
        clock_gettime(CLOCK_REALTIME, &now);                            \
        std::ostringstream text;                                        \
        text << (func) << " " << (status);                              \
        m_infoPV.push(now, std::string(text.str()));                    \
        ndsErrorStream(m_node) << std::string(text.str()) << std::endl; \
    } while(0)

#define SIS8300NDS_STATUS_CHECK(func, status)                                \
    do {                                                                    \
        if ((status) != status_success) {                                   \
            SIS8300NDS_STATUS_MSGERR(func, status);                          \
            /*throw nds::NdsError("ifcdaqdrv returned error");*/            \
        }                                                                   \
    } while (0)

/** Status check helper that prints error message from userspace library,
 * puts the object into ERROR state, sends a message to the MSGR record
 * and returns ndsError.*/
//TODO: Review. Check if is the best way
#define SIS8300NDS_STATUS_ASSERT(func, status)                                  \
        do {                                                                    \
            if ((status) != status_success) {                                   \
                SIS8300NDS_STATUS_MSGERR(func, status);                         \
            }                                                                   \
        } while (0)



//#define CHANNEL_NUMBER_MAX 8
//TODO: define correctly
#define CHANNEL_SAMPLES_MAX (4*1024*1024)


//-----------------------------------------------------------------------------
//  Round upwards to nearest power-of-2.
//-----------------------------------------------------------------------------
inline int ceil_pow2(unsigned number) {
    return ((val) + 0x1) & ~0x1;
}

//-----------------------------------------------------------------------------
//  Round upwards to nearest mibibytes
//-----------------------------------------------------------------------------
inline int ceil_mibi(unsigned number) {
	return (1 + (number - 1) / (1024 * 1024)) * 1024 * 1024;
}

#endif /* SIS8300_H */
