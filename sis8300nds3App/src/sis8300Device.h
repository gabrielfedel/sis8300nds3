#ifndef SIS8300DEVICE_H
#define SIS8300DEVICE_H

#include <initHooks.h>
#include <iocsh.h>
#include <epicsExport.h>

#include <nds3/nds.h>

#include <sis8300drv.h>
#include <sis8300llrfdrv.h>

#include "sis8300AIChannelGroup.h"
#include "sis8300AIChannel.h"
#include "sis8300FSM.h"
#include "sis8300.h"

class sis8300Device
{
  public:
    sis8300Device(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters);
    ~sis8300Device();


  private:
    std::shared_ptr<sis8300FSM> m_FSM;
    std::vector<std::shared_ptr<sis8300AIChannelGroup> > m_AIChannelGroup;
    sis8300drv_usr  *m_deviceUser;
    nds::Node m_node;
};

#endif /* SIS8300DEVICE_H */
