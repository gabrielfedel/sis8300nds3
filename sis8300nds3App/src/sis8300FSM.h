/**
 * @file sis8300FSM.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef SIS8300FSM_H
#define SIS8300FSM_H

#include "sis8300.h"
#include <sis8300drv.h>
#include <sis8300llrfdrv.h>
#include <nds3/nds.h>
#include <unistd.h>

class sis8300FSM {
public:
	sis8300FSM(const std::string &name, nds::Node &parentNode,
							sis8300drv_usr &deviceUser);

	~sis8300FSM();

	// Public parameters
	nds::Port m_node;
	nds::StateMachine m_stateMachine;
	sis8300drv_usr &m_deviceUser;
	

    void getInfoMessage(timespec *timespec, std::string *value);
    void getFirmwareVersion(timespec *timespec, std::string *value);

	// NDS State Machine Functions
	void onSwitchOn();
	void onSwitchOff();
	void onStart();
	void onStop();
	void recover();
	bool allowChange(const nds::state_t currentLocal,
									 const nds::state_t currentGlobal,
									 const nds::state_t nextLocal);
	

private:
	
    std::string m_firmwareVersion;
	// --------------------------------------------------------
	// PVs definitions
	// --------------------------------------------------------

	nds::PVDelegateIn<std::string> m_infoPV;
	nds::PVDelegateIn<std::string> m_firmwareVerPV;
};

#endif /* SIS8300FSM_H */
